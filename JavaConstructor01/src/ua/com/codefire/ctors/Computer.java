/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.ctors;

/**
 *
 * @author human
 */
public class Computer /*extends Object*/ {

    private int cpuCores;
    private double cpuFrequency;

//    public Computer() {
//        super(); // Invoke "Object" constructor
//    }
    // Custom constructor
    public Computer(int cpuCores, double cpuFrequency) {
        super();
        //this.field_name = param_name;
        this.cpuCores = cpuCores;
        this.cpuFrequency = cpuFrequency;
    }

    public int getCpuCores() {
        return cpuCores;
    }

    public double getCpuFrequency() {
        return cpuFrequency;
    }

    public void setCpuFrequency(double cpuFrequency) {
        if (cpuFrequency <= 5.0) {
            this.cpuFrequency = cpuFrequency;
        } else {
            this.cpuFrequency = 5.;
        }
    }

    @Override
    public String toString() {
        return "Computer{" + "cpuCores=" + cpuCores + ", cpuFrequency=" + cpuFrequency + '}';
    }
}
