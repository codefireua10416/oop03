/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.ctors;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
//        Computer comp1 = new Computer();
//        comp1.setCores(4);
//        comp1.setFrequency(2.54);
        Computer comp2 = new Computer(2, 4.20);
        comp2.setCpuFrequency(6.);
        
//        System.out.println(comp1);
        System.out.println(comp2);
        
        comp2.setCpuFrequency(4.99);
        System.out.println(comp2);
    }
    
}
