/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Laptop extends Computer {
    
    private Display display;
    private Keyboard keyboard;
    private Touchpad touchpad;

//    public Laptop() {
//        super();
//    }

    public Laptop(Display display, Keyboard keyboard, Touchpad touchpad, Motherboard motherboard, Power power) {
        super(motherboard, power);
        
        this.display = display;
        this.keyboard = keyboard;
        this.touchpad = touchpad;
    }

    public Display getDisplay() {
        return display;
    }

    public void setDisplay(Display display) {
        this.display = display;
    }

    public Keyboard getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(Keyboard keyboard) {
        this.keyboard = keyboard;
    }

    public Touchpad getTouchpad() {
        return touchpad;
    }

    public void setTouchpad(Touchpad touchpad) {
        this.touchpad = touchpad;
    }
    
}
