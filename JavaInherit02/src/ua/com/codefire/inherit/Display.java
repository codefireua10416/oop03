/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Display {
    
    private String matrixType;
    /**
     * Diagonal in inches.
     */
    private double diagonal;

    public Display(String matrixType, double diagonal) {
        this.matrixType = matrixType;
        this.diagonal = diagonal;
    }

    public String getMatrixType() {
        return matrixType;
    }

    /**
     * Return display diagonal in inches.
     * @return value of inches.
     */
    public double getDiagonal() {
        return diagonal;
    }
}
