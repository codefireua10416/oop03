/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

import java.util.Arrays;

/**
 *
 * @author human
 */
public class Motherboard {
    
    private Processor cpu;
    private Memory[] rams;

    public Motherboard(Processor cpu, Memory[] ram) {
        this.cpu = cpu;
        this.rams = ram;
    }

    public Processor getCpu() {
        return cpu;
    }

    public Memory[] getRams() {
        return rams;
    }

    @Override
    public String toString() {
        return "Motherboard{" + "cpu=" + cpu + ", ram=" + Arrays.toString(rams) + '}';
    }
}
