/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Touchpad {
    
    private String type;
    private int points;

    public Touchpad(String type, int points) {
        this.type = type;
        this.points = points;
    }

    public String getType() {
        return type;
    }

    public int getPoints() {
        return points;
    }
}
