/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Memory m1 = new Memory(8, 1600);
//        Memory m2 = new Memory(8, 1600);

        Memory[] mems = {
            new Memory(8, 1600),
            new Memory(8, 1600)
        };

        Motherboard mb = new Motherboard(new Processor(4, 2.45, 1, 4), mems);
        
        Computer comp1 = new Computer(mb, new Power(400));
        
        System.out.println(comp1);
        
        
        Display tft15 = new Display("tft", 15);
        Touchpad capacitiveTouchpad = new Touchpad("cpacitive", 5);
        Keyboard keyboard = new Keyboard("EN:RU:UA", 101);
        
        Laptop laptop = new Laptop(tft15, keyboard, capacitiveTouchpad, mb, new Power(300));
    }

}
