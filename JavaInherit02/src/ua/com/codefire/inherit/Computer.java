/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Computer /*extends Object*/ {

    private Motherboard motherboard;
    private Power power;

//    public Computer() {
//        super();
//    }

    public Computer(Motherboard motherboard, Power power) {
        this.motherboard = motherboard;
        this.power = power;
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(Motherboard motherboard) {
        this.motherboard = motherboard;
    }

    public Power getPower() {
        return power;
    }

    public void setPower(Power power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Computer{" + "motherboard=" + motherboard + ", power=" + power + '}';
    }
}
