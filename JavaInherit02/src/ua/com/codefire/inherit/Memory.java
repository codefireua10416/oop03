/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Memory {

    private int capacity;
    private int frequency;

    public Memory(int capacity, int frequency) {
        this.capacity = capacity;
        this.frequency = frequency;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "Memory{" + "capacity=" + capacity + ", frequency=" + frequency + '}';
    }
}
