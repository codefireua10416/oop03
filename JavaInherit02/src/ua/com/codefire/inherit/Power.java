/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Power {
    
    private int supply;

    public Power(int supply) {
        this.supply = supply;
    }

    public int getSupply() {
        return supply;
    }

    @Override
    public String toString() {
        return "Power{" + "supply=" + supply + '}';
    }
}
