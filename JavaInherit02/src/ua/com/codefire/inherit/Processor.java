/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Processor {

    private int cores;
    private double frequency;
    private int cacheL1;
    private int cacheL2;

    public Processor(int cores, double frequency, int cacheL1, int cacheL2) {
        this.cores = cores;
        this.frequency = frequency;
        this.cacheL1 = cacheL1;
        this.cacheL2 = cacheL2;
    }

    public int getCores() {
        return cores;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public int getCacheL1() {
        return cacheL1;
    }

    public int getCacheL2() {
        return cacheL2;
    }

    @Override
    public String toString() {
        return "Processor{" + "cores=" + cores + ", frequency=" + frequency + ", cacheL1=" + cacheL1 + ", cacheL2=" + cacheL2 + '}';
    }
}
