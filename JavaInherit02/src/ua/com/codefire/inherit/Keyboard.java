/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Keyboard {
    
    private String locale;
    private int keysCount;

    public Keyboard(String locale, int keysCount) {
        this.locale = locale;
        this.keysCount = keysCount;
    }

    public String getLocale() {
        return locale;
    }

    public int getKeysCount() {
        return keysCount;
    }
}
