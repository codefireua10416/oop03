/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.inherit;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        A a1 = new A();

        a1.setA(56);
        System.out.println(a1.getA());
        
        B b1 = new B();
        b1.setA(32);
        b1.setB(12);
        
        System.out.println(b1.getA());
        System.out.println(b1.getB());
    }

}
